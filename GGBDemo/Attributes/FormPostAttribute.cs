﻿using System;


namespace GGBDemo.Attributes
{
    public class FormPostAttribute:Attribute
    {

        public FormPostAttribute(string actionName,string controllerName)
        {
            ActionName = actionName;
            ControllerName = controllerName;
        }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
    }

}