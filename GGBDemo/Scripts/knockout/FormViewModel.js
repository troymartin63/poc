﻿var FormViewModel = function() {
    var self = this;
    self.ActionName = ko.observable("");
    self.Method = ko.observable("");
    self.AccessToken = ko.observable("");
    self.FormElements = ko.observableArray([]);

    self.ActionName = ko.pureComputed(function () {
        var actionName = "";
        $.grep(self.FormElements(),
            function(el) {
                if (el.type == 'postAttribute') {
                    actionName = el.actionName;
                    return true;
                }
                return false;
            });
        return actionName;
    });
    self.SubmitText = ko.pureComputed(function () {
        return $.grep(self.FormElements(),
            function (el) {
                if (el.name === 'SubmitText') {
                    return el.value;
                }
                return '';
            });
    });
    self.TotalStake = ko.pureComputed(function () {
        var totalStake = "";
        $.grep(self.FormElements(),
            function (el) {
                if (el.name === 'Stake') {
                    totalStake = el.value;
                    return true;
                }
                return false;
            });
        return totalStake;
    });
    self.Stake = function () {
        var stake = "";
        $.grep(self.FormElements(),
            function (el) {
                if (el.name === 'Stake') {
                    stake = el.value;
                    return true;
                }
                return false;
            });
        return stake;
    };
    
    self.ControllerName = ko.pureComputed(function () {
        var controllerName = "";
        $.grep(self.FormElements(),
            function (el) {
                if (el.type == 'postAttribute') {
                    controllerName = el.controllerName;
                    return true;
                }
                return false;
            });
        return controllerName;
    });

}