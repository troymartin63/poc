﻿/// <reference path="knockout-3.4.2.debug.js" />
/// <reference path="~/Scripts/knockout/FormViewModel.js" />
/// <reference path="~/Scripts/knockout.mapping.js" />
/// <reference path="~/Scripts/jquery-1.10.2.intellisense.js" />
var viewModels;
$(function() {
    viewModels = {};
    if (typeof FormViewModel != 'undefined')
        viewModels.FormViewModel = new FormViewModel();

    
    ko.applyBindings(viewModels);

    $('.load').on("click", function () {
        var model = $('#viewModels option:selected').text();
        $(".jsonForm").each(function () {
            $.ajax({
                url: "Forms/GetModel?modelName=" + model,
                type: "GET",
                success: function (data) {
                    if (data !== 'Please log in') {
                        console.log("got the model");
                        viewModels.FormViewModel.FormElements(data);
                    } else alert('please log in');
                },
                error: function (data) {

                }
            });
        });
        
    });

    $(".jsonForm").submit(function (e) {
        $.ajax({
            url: viewModels.FormViewModel.ControllerName() + '/'+ viewModels.FormViewModel.ActionName(),
            data: $(this).serialize(),
            type: "POST",
            success: function (data) {
                window.location.reload(true);
            },
            error: function (data) {

            }
        });
        e.stopPropagation();
        return false;
    });
});



