﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using GGBDemo.Models;


namespace GGBDemo.Controllers
{
    public class FormsController : BaseController
    {

        public ActionResult Index()
        {
            var typelist = GetTypesInNamespace(Assembly.GetExecutingAssembly(), "GGBDemo.Models");
            var viewModels = typelist.Select(t => new SelectListItem() {Selected = false,Text=t.Name,Value=t.Name}).ToList();
            ViewBag.ViewModels = viewModels;
            return View();
        }

        [HttpGet]
      
        public ActionResult GetModel(string modelName)
        {
            try
            {
                if(!User.Identity.IsAuthenticated && (modelName != "LoginViewModel" && modelName != "RegisterViewModel"))
                   return Content("Please log in");
                var type = Type.GetType("GGBDemo.Models." + modelName, true);
                var model = Activator.CreateInstance(type);
                return JsonWithAttribute(model);
            }
            catch(Exception)
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult PostModel(FormCollection formElements)
        {
            
          //read model and save and return result
            return null;
        }

        private Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return assembly.GetTypes().Where(t => String.Equals(t.Namespace, nameSpace, StringComparison.Ordinal)).ToArray();
        }
    }
}