﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Http.Headers;
using System.Reflection;
using GGBDemo.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebGrease.Css.Extensions;

namespace GGBDemo.JsonConverters
{
    /// <summary>
    /// Complext type converter
    /// This class will convert attributes to json properties
    /// </summary>
    public class ComplexTypeConverter : JsonConverter
    {
       
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var rootObject = Activator.CreateInstance(objectType);
            var objJson = JToken.ReadFrom(reader);

            foreach (var token in objJson)
            {
                var propInfo = rootObject.GetType().GetProperty(token.Path, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (!propInfo.CanWrite) continue;
                var tk = token as JProperty;
                if (tk?.Value is JObject)
                {
                    var val = tk.Value.SelectToken("value") as JValue;
                    if (val != null)
                        propInfo.SetValue(rootObject, Convert.ChangeType(val.Value, propInfo.PropertyType.UnderlyingSystemType), null);
                }
                else
                {
                    if (tk != null)
                        propInfo.SetValue(rootObject, Convert.ChangeType(tk.Value, propInfo.PropertyType.UnderlyingSystemType), null);
                }
            }
            return rootObject;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var jsonarray = new JArray();
            
            var type = value.GetType();
            var joType = new JObject();
            joType.Add("ModelName", JToken.FromObject(type.Name, serializer));
            joType.Add("type", JToken.FromObject("Model", serializer));
            jsonarray.Add(joType);
            foreach (var propInfo in type.GetProperties())
            {
                var jo = new JObject();
                if (!propInfo.CanRead) continue;
                var propVal = propInfo.GetValue(value, null);

                jo.Add("name", JToken.FromObject(propInfo.Name, serializer));
                jo.Add("value", JToken.FromObject(propVal ?? string.Empty, serializer));
                var propTypeName = propInfo.PropertyType.Name.ToLower();
                switch (propTypeName)
                {
                        case "string":
                        jo.Add("type", JToken.FromObject("text", serializer));
                        break;

                        case "boolean":
                            jo.Add("type", JToken.FromObject("bool", serializer));
                            break;

                        case "single":
                            jo.Add("type", JToken.FromObject("float", serializer));
                            break;

                    case "decimal":
                        jo.Add("type", JToken.FromObject("decimal", serializer));
                        break;
                    case "int32":
                        jo.Add("type", JToken.FromObject("int", serializer));
                        break;
                }
                
                var cutomAttributes = propInfo.GetCustomAttributes();
                
                cutomAttributes.ForEach(customAttribute =>
                {
                    var attrName = customAttribute.GetType().Name.Replace("Attribute", "");
                    if (customAttribute == null) return;
                    if (customAttribute is RequiredAttribute)
                    {
                        jo.Add("required",JToken.FromObject(true, serializer));
                    }
                    else if (customAttribute is DataTypeAttribute)
                    {
                        jo.Add("dataType",
                            JToken.FromObject(
                                ((DataTypeAttribute)customAttribute).DataType.ToString().ToLowerInvariant(),
                                serializer));

                        if (((DataTypeAttribute) customAttribute).DataType.ToString() == "Text")
                        {
                            jo.Remove("type");
                            jo.Add("type", JToken.FromObject("label", serializer));
                        }
                    }
                    else if (customAttribute is DisplayAttribute)
                    {
                        jo.Add(attrName,
                            JToken.FromObject(((DisplayAttribute)customAttribute).Name , serializer));
                    }
                    
                    
                });
                jsonarray.Add(jo);
            }

            var formPostAttribute = type.GetCustomAttribute<FormPostAttribute>();
            if (formPostAttribute != null)
            {
                var jo = new JObject();
                jo.Add("type", JToken.FromObject("postAttribute", serializer));
                jo.Add("actionName",
                            JToken.FromObject(formPostAttribute.ActionName , serializer));
                jo.Add("controllerName",
                            JToken.FromObject(formPostAttribute.ControllerName, serializer));
                jsonarray.Add(jo);
            }
            jsonarray.WriteTo(writer);
        }
    }
}