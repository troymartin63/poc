﻿
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using GGBDemo.Attributes;
using GGBDemo.JsonConverters;
using Newtonsoft.Json;

namespace GGBDemo.Models
{
    [JsonConverter(typeof(ComplexTypeConverter))]
    [FormPost("PostModel", "Forms")]
    public class BetSlipViewModel
    {
        [HiddenInput]
        public int BetId { get; set; }
        [DataType(DataType.Text)]
        public string MatchName { get; set; }
        [DataType(DataType.Text)]
        public string Description { get; set; }
        [DataType(DataType.Text)]
        public decimal Price { get; set; }
        [DataType(DataType.Text)]
        public decimal MinimumStake { get; set; }
        public float Stake { get; set; }
        
       
        public BetSlipViewModel()
        {
            MatchName = "Pittsburgh Steelers";
            Description = "American Football: Championship Outrights: NFL AFC Division North - Winner: Championship Outright 31 Dec at 22:00";
            MinimumStake = 1;
            Price = 0.75m;
           
        }

    }
}